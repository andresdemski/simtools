
import cocotb
import random
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge

@cocotb.test()
def test_flipflop(dut):
    cocotb.fork(Clock(dut.clk, 10, 'ns').start())
    dut.d <= 0
    yield RisingEdge(dut.clk)
    yield RisingEdge(dut.clk)

    for _ in range(1000):
        value = random.randint(0, 1)
        dut.d <= value
        yield RisingEdge(dut.clk)
        yield RisingEdge(dut.clk)
        assert value == dut.q
