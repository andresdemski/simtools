library work;
use std.env.stop;

library ieee;
use ieee.std_logic_1164.all;

entity tb_flipflop is
end tb_flipflop;

architecture behavioral of tb_flipflop is
    signal clk: std_logic;
    signal q: std_logic;
    signal d: std_logic;
    
    begin
    dut: entity work.flipflop port map (clk => clk, q => q, d => d);
    
    clock_process :process
    begin
         clk <= '0';
         wait for 10 ns;
         clk <= '1';
         wait for 10 ns;
    end process;
    
    stymulus :process
    begin
        d <= '0';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        for I in 1 to 100 loop
            d <= not q;
            wait until rising_edge(clk);
            wait until rising_edge(clk);
            assert d = q;
        end loop;
        stop; -- Finish
    end process;
end behavioral;
