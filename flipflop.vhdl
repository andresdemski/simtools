library ieee;
use ieee.std_logic_1164.all;

entity flipflop is 
    port(
        q : out std_logic;
        clk :in std_logic;
        d :in  std_logic    
    );
end flipflop;

architecture behavioral of flipflop is  
begin  
    process(clk) begin 
        if(rising_edge(clk)) then
            q <= d; 
        end if;       
    end process;  
end behavioral; 
