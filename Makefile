TOPLEVEL ?= tb_flipflop

sim:
	ghdl -i --std=08 *.vhdl
	ghdl -m --std=08 --ieee=synopsys  $(TOPLEVEL)
	ghdl -r --std=08 --ieee=synopsys $(TOPLEVEL) --wave=$(TOPLEVEL).ghw # --stop-time=1300us

clean:
	-rm -f *.o $(toplevel) $(toplevel).ghw work-obj08.cf
